package comparative;

import utils.ImageComparativeLogger;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 8/17/13
 * Time: 9:56 AM
 */
public class ImageComparativeCallbackImpl implements ImageComparativeCallback {

    @Override
    public void onComplete(BufferedImage resultImage) throws IOException {
        ImageComparativeLogger.printMessage("Compare complete");
        if (resultImage != null) {

            createDirIfNeeded("output");

            File outputFile = new File("output/image_difference.png");
            ImageIO.write(resultImage, "png", outputFile);
        } else {
            ImageComparativeLogger.printMessage("There are no differences between images");
        }
    }

    private void createDirIfNeeded(String path) {
        File file = new File(path);
        if (file.exists() && !file.isDirectory()) {
            file.delete();
        }
        if (!file.exists()) {
            file.mkdirs();
        }
    }
}