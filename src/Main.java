import comparative.ImageComparativeCallbackImpl;
import comparative.ImageComparativeImpl;
import utils.AppLogger;
import utils.ImageComparativeSettings;

import java.io.Console;

/**
 * Created with IntelliJ IDEA.
 * User: Chernyshov Yuriy
 * Date: 7/15/13
 * Time: 10:50 PM
 */
public class Main {

    public static void main(String[] arguments) {
        new Main(arguments);
    }

    public Main(String[] arguments) {
        Console console = System.console();
        if (console != null) {
            String resultDestination = console.readLine("Start image comparison Y / N: ");
            if (resultDestination != null) {
                if (resultDestination.equalsIgnoreCase("Y")) {
                    try {
                        doProcess(arguments);
                    } catch (Exception e) {
                        AppLogger.printError("Can not process images: " + e.getMessage());
                    }
                } else if (resultDestination.equalsIgnoreCase("N")) {
                    System.exit(0);
                }
            }
        }
        // For the local usage only
        try {
            doProcess(arguments);
        } catch (Exception e) {
            AppLogger.printError("Can not process images: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void doProcess(String[] arguments) throws Exception {
        AppLogger.printMessage("Parse incoming arguments ...");

        ImageComparativeSettings settings = new ImageComparativeSettings();
        settings.parseArguments(arguments);

        String sourceImagePath = "assets/images/1.jpg";
        String imageToCompareWithPath = "assets/images/2.jpg";

        ImageComparativeImpl imageComparative = new ImageComparativeImpl(settings);
        ImageComparativeCallbackImpl callback = new ImageComparativeCallbackImpl();
        imageComparative.setCallback(callback);
        imageComparative.compare(sourceImagePath, imageToCompareWithPath);
    }
}