package graph;

import utils.ImageComparativeLogger;

import java.util.HashMap;
import java.util.LinkedList;

/**
 * Created by ChernyshovYuriy on 16.08.13
 */
public class GraphManager {

    private Graph graph;

    public GraphManager(Graph graph) {
        this.graph = graph;
    }

    public boolean hasDifferences() {
        return graph.getVertexesSize() > 0;
    }

    public HashMap<Integer, LinkedList<Vertex>> getDifferencesRegions() {
        ImageComparativeLogger.printMessage("Start differences regions computing ...");

        HashMap<Integer, LinkedList<Vertex>> hashMap = new HashMap<Integer, LinkedList<Vertex>>();
        LinkedList<Vertex> linkedList;

        Vertex vertex;
        for (int i = 0; i < graph.getVertexesSize(); i++) {
            vertex = graph.getVertexAt(i);
            //ImageComparativeLogger.printMessage("TRACE " + vertex.getLeader());
            linkedList = hashMap.get(vertex.getLeader());
            if (linkedList == null) {
                linkedList = new LinkedList<Vertex>();
                hashMap.put(vertex.getLeader(), linkedList);
            }
            linkedList.add(vertex);
        }

        ImageComparativeLogger.printMessage("Differences regions computing complete, regions number: " + hashMap.size());
        return hashMap;
    }

    public void computeAdjacencies() {
        long startTime = System.currentTimeMillis();

        ImageComparativeLogger.printMessage("Start compute adjacencies ...");

        Vertex currentVertex;
        Vertex nextVertex;
        int leader = 0;
        double cartesianDistance;
        double xDifferences;
        double yDifferences;
        for (int i = 0; i < graph.getVertexesSize(); i++) {
            currentVertex = graph.getVertexAt(i);
            if (currentVertex.getLeader() == -1) {
                currentVertex.setLeader(leader++);
            }
            for (int j = 0; j < graph.getVertexesSize(); j++) {
                if (i == j) {
                    continue;
                }

                nextVertex = graph.getVertexAt(j);

                if (currentVertex.getLeader() == nextVertex.getLeader()) {
                    continue;
                }

                xDifferences = Math.abs(currentVertex.getXPosition() - nextVertex.getXPosition());
                yDifferences = Math.abs(currentVertex.getYPosition() - nextVertex.getYPosition());
                cartesianDistance = Math.sqrt(Math.pow(xDifferences, 2) + Math.pow(yDifferences, 2));

                if (cartesianDistance <= 20 ) {
                    nextVertex.setLeader(currentVertex.getLeader());
                }
            }
        }

        ImageComparativeLogger.printMessage("running time: " + (System.currentTimeMillis() - startTime) + " ms," +
                "leaders number: " + leader);
    }
}