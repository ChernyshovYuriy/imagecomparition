package graph;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 8/14/13
 * Time: 7:44 PM
 */
public class Graph {

    private List<Vertex> vertexes;

    public Graph() {
        vertexes = new ArrayList<Vertex>();
    }

    public void addVertex(Vertex vertex) {
        vertexes.add(vertex);
    }

    public Vertex getVertexAt(int position) {
        return vertexes.get(position);
    }

    public int getVertexesSize() {
        return vertexes.size();
    }
}