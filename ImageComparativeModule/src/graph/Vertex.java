package graph;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 8/14/13
 * Time: 7:44 PM
 */
public class Vertex {

    private int leader = -1;
    private int xPosition;
    private int yPosition;

    public Vertex(int xPosition, int yPosition) {
        this.xPosition = xPosition;
        this.yPosition = yPosition;
    }

    public int getLeader() {
        return leader;
    }

    public void setLeader(int leader) {
        this.leader = leader;
    }

    public int getXPosition() {
        return xPosition;
    }

    public int getYPosition() {
        return yPosition;
    }
}