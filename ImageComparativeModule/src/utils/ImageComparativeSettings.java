package utils;

import java.util.ArrayList;

/**
 * Created by ChernyshovYuriy on 16.08.13
 */
public class ImageComparativeSettings {

    /**
     * Difference in RGB value between two pixels
     */
    private static final int RGB_PERCENTAGE_DIFFERENCE = 10;

    private static final String IGNORING_ARGUMENT = "ignoring";
    private static final String COLORS_TOLERANCE_ARGUMENT = "colors_tolerance";

    /**
     * Provides a “tolerance” comparison parameters array that allows to treat similar colors as the same
     * (2 shades of bright red with less than 10% distance are not shown as differences, for example).
     * Contains of 3 items for the Red, Green and Blue colors accordingly.
     * toleranceColorsCompareParameters[0] - Red
     * toleranceColorsCompareParameters[1] - Green
     * toleranceColorsCompareParameters[2] - Blue
     */
    private int[] toleranceColorsCompareParameters;

    /**
     * provides a possibility to exclude certain parts of the image from comparison, for example a clock or
     * dynamically generated number. Provided as a list of rectangles to exclude.
     */
    private ArrayList<int[]> excludeRegionsData;

    public ImageComparativeSettings() {
        toleranceColorsCompareParameters = new int[]{RGB_PERCENTAGE_DIFFERENCE,
                                                     RGB_PERCENTAGE_DIFFERENCE,
                                                     RGB_PERCENTAGE_DIFFERENCE};
        excludeRegionsData = new ArrayList<int[]>();
    }

    public int[] getToleranceColorsCompareParameters() {
        return toleranceColorsCompareParameters;
    }

    public void setToleranceColorsCompareParameters(int[] toleranceColorsCompareParameters) {
        this.toleranceColorsCompareParameters = toleranceColorsCompareParameters;
    }

    public ArrayList<int[]> getExcludeRegionsData() {
        return excludeRegionsData;
    }

    public boolean hasExcludeRegions() {
        return excludeRegionsData.size() > 0;
    }

    public void parseArguments(String[] arguments) {
        for (String argument: arguments) {
            ImageComparativeLogger.printMessage(" - " + argument);
            String[] strings;
            if (argument.startsWith(IGNORING_ARGUMENT)) {
                ImageComparativeLogger.printMessage("Process '" + IGNORING_ARGUMENT + "'");
                argument = argument.substring(IGNORING_ARGUMENT.length() + 1, argument.length());
                strings = argument.split(";");
                int[] excludeRegions;
                String x, y, width, height;
                for (String k : strings) {
                    if (k.split(",").length == 4) {
                        x = k.split(",")[0];
                        y = k.split(",")[1];
                        width = k.split(",")[2];
                        height = k.split(",")[3];
                        if (isNumeric(x) && isNumeric(y) && isNumeric(width) && isNumeric(height)) {
                            excludeRegions = new int[]{Integer.valueOf(x), Integer.valueOf(y),
                                    Integer.valueOf(width), Integer.valueOf(height)};
                            addItemToExcludeRegionsData(excludeRegions);
                        } else {
                            ImageComparativeLogger.printError("Can not parse 'ignoring' argument");
                        }
                    } else {
                        ImageComparativeLogger.printError("Can not parse 'ignoring' argument");
                    }
                }
            }
            if (argument.startsWith(COLORS_TOLERANCE_ARGUMENT)) {
                ImageComparativeLogger.printMessage("Process '" + COLORS_TOLERANCE_ARGUMENT + "'");
                argument = argument.substring(COLORS_TOLERANCE_ARGUMENT.length() + 1, argument.length());
                strings = argument.split(",");
                int toleranceValue;
                int counter = 0;
                for (String k : strings) {
                    toleranceValue = Integer.valueOf(k);
                    if (!isNumeric(k) || toleranceValue < 0 || toleranceValue > 100) {
                        toleranceValue = RGB_PERCENTAGE_DIFFERENCE;
                    }
                    addItemToColorsToleranceData(counter++, toleranceValue);
                }
                ImageComparativeLogger.printMessage("Colors tolerances: " + toleranceColorsCompareParameters[0] + " " +
                        toleranceColorsCompareParameters[1] + " " + toleranceColorsCompareParameters[2]);
            }
        }
        if (!hasExcludeRegions()) {
            ImageComparativeLogger.printWarning("There are no exclude regions specified");
        }
    }

    private void addItemToExcludeRegionsData(int[] item) {
        excludeRegionsData.add(item);
    }

    private void addItemToColorsToleranceData(int position, int item) {
        toleranceColorsCompareParameters[position] = item;
    }

    private boolean isNumeric(String str)  {
        return str.matches("-?\\d+(\\.\\d+)?");
    }
}