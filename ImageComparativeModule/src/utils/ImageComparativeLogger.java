package utils;

/**
 * Created by ChernyshovYuriy on 16.08.13
 */
public class ImageComparativeLogger {

    public static void printWarning(String message) {
        System.out.println("[W] " + message);
    }

    public static void printError(String message) {
        System.out.println("[E] " + message);
    }

    public static void printMessage(String message) {
        System.out.println("[I] " + message);
    }
}