package comparative;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 8/17/13
 * Time: 9:49 AM
 */
public interface ImageComparative {

    public void compare(String sourceImagePath, String imageToCompareWithPath) throws Exception;
}