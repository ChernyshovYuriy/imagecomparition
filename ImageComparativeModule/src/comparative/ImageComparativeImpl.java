package comparative;

import graph.Graph;
import graph.GraphManager;
import graph.Vertex;
import utils.ImageComparativeLogger;
import utils.ImageComparativeSettings;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 8/16/13
 * Time: 9:09 PM
 */
public class ImageComparativeImpl implements ImageComparative {

    private ImageComparativeCallback callback;
    private ImageComparativeSettings settings;

    public ImageComparativeImpl(ImageComparativeSettings settings) {
        if (settings == null) {
            ImageComparativeLogger.printError(getClass().getSimpleName() + " constructor, Application Settings must be specified");
            throw new NullPointerException();
        }
        this.settings = settings;
    }

    public static BufferedImage loadImage(String fileName) throws IOException {
        return ImageIO.read(new File(fileName));
    }

    @Override
    public void compare(String sourceImagePath, String imageToCompareWithPath) throws Exception {
        if (sourceImagePath == null) {
            ImageComparativeLogger.printError(getClass().getSimpleName() + " compare() method, Source Image URL must not be null");
            throw new NullPointerException();
        }
        if (imageToCompareWithPath == null) {
            ImageComparativeLogger.printError(getClass().getSimpleName() + " compare() method," +
                    "'Image to Compare with' URL must not be null");
            throw new NullPointerException();
        }
        if (sourceImagePath.equals("")) {
            ImageComparativeLogger.printError(getClass().getSimpleName() + " compare() method, Source Image URL must not be empty");
            throw new Exception();
        }
        if (imageToCompareWithPath.equals("")) {
            ImageComparativeLogger.printError(getClass().getSimpleName() + " compare() method," +
                    "'Image to Compare with' URL must not be empty");
            throw new Exception();
        }
        BufferedImage sourceImage = loadImage(sourceImagePath);
        BufferedImage imageToCompareWith = loadImage(imageToCompareWithPath);
        BufferedImage imageDifference = loadImage(imageToCompareWithPath);

        if (settings.hasExcludeRegions()) {
            drawExcludeRegions(imageDifference);
        }

        Graph graph = doComparing(sourceImage, imageToCompareWith);
        GraphManager graphManager = new GraphManager(graph);
        if (graphManager.hasDifferences()) {
            graphManager.computeAdjacencies();
            Graphics2D graphics2D = imageDifference.createGraphics();
            HashMap<Integer, LinkedList<Vertex>> differencesRegions = graphManager.getDifferencesRegions();
            drawDifferences(differencesRegions, graphics2D);

            if (callback != null) {
                callback.onComplete(imageDifference);
            }
        } else {
            if (callback != null) {
                callback.onComplete(null);
            }
        }
    }

    public void setCallback(ImageComparativeCallback callback) {
        this.callback = callback;
    }

    private Graph doComparing(BufferedImage sourceImage, BufferedImage imageToCompareWith) {
        int compare_X = sourceImage.getWidth();
        int compare_Y = sourceImage.getHeight();

        ImageComparativeLogger.printMessage("Start comparing ...");

        Graph graph = new Graph();

        for (int y = 0; y < compare_Y; y++) {
            for (int x = 0; x < compare_X; x++) {
                if (isUnderExcludeRegion(x, y)) {
                    continue;
                }

                int sourceImageRGB = sourceImage.getRGB(x, y);
                int imageToCompareWithRGB = imageToCompareWith.getRGB(x, y);

                int sourceImageR = (sourceImageRGB >> 16) & 0xff;
                int sourceImageG = (sourceImageRGB >> 8) & 0xff;
                int sourceImageB = (sourceImageRGB) & 0xff;

                int imageToCompareWithR = (imageToCompareWithRGB >> 16) & 0xff;
                int imageToCompareWithG = (imageToCompareWithRGB >> 8) & 0xff;
                int imageToCompareWithB = (imageToCompareWithRGB) & 0xff;

                int redColorDifference = settings.getToleranceColorsCompareParameters()[0];
                int greenColorDifference = settings.getToleranceColorsCompareParameters()[1];
                int blueColorDifference = settings.getToleranceColorsCompareParameters()[2];

                if (colorDifference(sourceImageR, imageToCompareWithR) >= redColorDifference &&
                        colorDifference(sourceImageG, imageToCompareWithG) >= greenColorDifference &&
                        colorDifference(sourceImageB, imageToCompareWithB) >= blueColorDifference) {

                    graph.addVertex(new Vertex(x, y));
                }
            }
        }

        ImageComparativeLogger.printMessage("Graph calculated");

        return graph;
    }

    private void drawDifferences(HashMap<Integer, LinkedList<Vertex>> hashMap, Graphics2D graphics2D) {
        ImageComparativeLogger.printMessage("Start draw differences ...");

        int min_X;
        int max_X;
        int min_Y;
        int max_Y;

        graphics2D.setColor(Color.RED);

        Iterator<Map.Entry<Integer,LinkedList<Vertex>>> iterator = hashMap.entrySet().iterator();
        Map.Entry<Integer, LinkedList<Vertex>> entry;
        LinkedList<Vertex> linkedList;
        Vertex vertex;
        while (iterator.hasNext()) {
            entry = iterator.next();
            linkedList = entry.getValue();
            min_X = Integer.MAX_VALUE;
            max_X = Integer.MIN_VALUE;
            min_Y = Integer.MAX_VALUE;
            max_Y = Integer.MIN_VALUE;
            for (Vertex groupedVertex : linkedList) {
                vertex = groupedVertex;
                if (vertex.getXPosition() > max_X) {
                    max_X = vertex.getXPosition();
                }
                if (vertex.getXPosition() < min_X) {
                    min_X = vertex.getXPosition();
                }
                if (vertex.getYPosition() > max_Y) {
                    max_Y = vertex.getYPosition();
                }
                if (vertex.getYPosition() < min_Y) {
                    min_Y = vertex.getYPosition();
                }
            }
            graphics2D.drawRect(min_X, min_Y, max_X - min_X, max_Y - min_Y);
        }

        ImageComparativeLogger.printMessage("Draw differences complete");
    }

    protected int colorDifference(int sourceColor, int colorToCompare) {
        if (sourceColor == colorToCompare) {
            return 0;
        }
        int maxValue;
        int minValue;
        if (sourceColor > colorToCompare) {
            maxValue = sourceColor;
            minValue = colorToCompare;
        } else {
            minValue = sourceColor;
            maxValue = colorToCompare;
        }
        if (maxValue == 0 && minValue > 0) {
            maxValue = 255;
        }
        double difference_a = maxValue - minValue;
        double difference_b = difference_a / maxValue;
        int difference = (int) (difference_b * 100);

        //ImageComparativeLogger.printMessage("A:" + sourceColor + " B:" + colorToCompare + " C:" + difference);

        return difference;
    }

    private boolean isUnderExcludeRegion(int x, int y) {
        if (!settings.hasExcludeRegions()) {
            return Boolean.FALSE;
        }
        /**
         * excludeRegion[0] = x
         * excludeRegion[1] = y
         * excludeRegion[2] = width
         * excludeRegion[3] = height
         */
        for (int[] excludeRegion: settings.getExcludeRegionsData()) {
            if (x >= excludeRegion[0] && x <= excludeRegion[0] + excludeRegion[2] &&
                    y >= excludeRegion[1] && y <= excludeRegion[1] + excludeRegion[3]) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

    private void drawExcludeRegions(BufferedImage imageDifference) {
        ImageComparativeLogger.printMessage("Draw exclude regions");
        Graphics2D graphics2D_Blue = imageDifference.createGraphics();
        graphics2D_Blue.setColor(Color.BLUE);
        for (int[] excludeRegion : settings.getExcludeRegionsData()) {
            if (excludeRegion != null) {
                graphics2D_Blue.drawRect(excludeRegion[0], excludeRegion[1], excludeRegion[2], excludeRegion[3]);
            }
        }
    }
}