package comparative;

import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 8/17/13
 * Time: 9:54 AM
 */
public interface ImageComparativeCallback {
    public void onComplete(BufferedImage resultImage) throws IOException;
}